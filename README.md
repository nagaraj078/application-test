# GreenIT Application Challenge

This project is the seed files needed to create a GreenIT test application.  

Objective: This test will be a fully functional web application that will display the given data from a csv file, and allow full editing capabilties of the data.

## Application Version Requirements
1. Frontend must be in Anguar 10+
    - Angular 14 used for frontend. 
2. Backend must be in PHP 7.2+
    - PHP 7.4 used for backend.
3. You may include any npm modules needed.
    - npm used for required package installation.
4. Frontend should be built using npm, backend should be raw php files.
    - Frontend used npm as build tool and Backend used core PHP devided as Model,Controller and Rest API.

## Backend Requirements
1. The backend will provide data to the frontend through a RESTful API
    - The backend will provide data to frontend through Rest API.
2. The API will allow GET requests for getting all records. 
    - Created GET Request method to list all the records.
3. The API will allow POST requests for save, update, and delete.  
(The implementation for these can be mocked. You do not have to write to the data file.)
    - Allowed POST,PATCH,DELETE request method with respect to Save, update and delete 
4. The API response will be JSON formatted data.
    - All API response will be in JSON formatted data.
5. The data will be read from a provided data file.
    - Reading data from provided Data source file(data.csv). 

## Frontend Requirements
1. Data will be displayed in a table view.
   - Data displayed as table view.
   - Pagination option implemented in table view. 
2. Each column header in the table will correspond to a field name in the data file.
   - Each column header in the table will be field name which is provided in data file.  
3. Allow each row and field to be edited.
    - Allowed user to edit each row.
4. Allow record creation and deletion, either inline or modal.
    - Create record using Bootstrap model 
    - Delete option given and before delete getting confirmation from the user to delete record.
  
## Data
1. Data will be provided as csv text file.
   - Using PHP function reading source data from CSV file. 
2. The fist row of the data file will contain the field names.
   - First row of the data file taken as field name. 

## Tests
1. Any testing framework/runner will be allowed with explicit instructions. If you are looking for one to use, we use Codeception.
   - API Testing done using Codeception. 
   - Functional testing done using Codeception. 
2. Any type of tests, Unit, Functional, Acceptance, or API tests.
   - API Testing done using Codeception. 
   - Functional testing done using Codeception. 
3. The more test case coverage the better.
   - Possible test cases done.
  
 ## Extras
1. Implement a immutable state solution in front end (e.g. Redux, ngrx, Akita)
   - Tried to use, but not completly done.
2. Show different parent child communication styles within your application.
   - Used parent child communication  
3. Show or describe web server architecture used.
4. Extra points for making an easy comand line install or a Dockerfile to build the application
 ## Instalation Steps:
    A. Backend:
        1. Backend required php v7+ and composer(for codeception package).
        2. Put backend folder inside htdocs folder of apache and Run on http://localhost/backend/index.php
        3.ENDPOINTS:
           - GET    http://localhost/backend/index.php/order/list 
           - POST   http://localhost/backend/index.php/order/add
           - PATCH  http://localhost/backend/index.php/order/edit?id=5
           - DELETE http://localhost/backend/index.php/order/delete?id=12
    B. Frontend:
        1. Frontend required node package manager(NPM latest) and angular CLI. 
        2. To install dependency package use "npm install" command in the project folder.   
        3. To Run the application use "ng serve" command. 
        4. Application visible on port http://localhost:4200/   
5. Make the API fully functional, and write to the CSV file.
   - Given feasibility of fully functional to write to the csv file.

	
