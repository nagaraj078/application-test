import { Component, OnInit,Input, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { OrderDetails, OrderDetailsColumns } from '../model/OrderDetails';
import { OrderDetailsService } from '../services/orderDetails.service';
import { AddOrderDetailsComponent } from '../add-order-details/add-order-details.component';
import { FormControl, Validators } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit{
  displayedColumns: string[] = OrderDetailsColumns.map((col) => col.key);
  columnsSchema: any = OrderDetailsColumns;
  dataSource = new MatTableDataSource<OrderDetails>();
  valid: any = {}; lastID: number = 0;  parentSearchedKey = ''; 

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  public searchStringDisplay = '';
  constructor(private route: ActivatedRoute, private router:Router, public dialog: MatDialog, private orderDetailsService: OrderDetailsService) {}

  ngOnInit() {
    //Get All the order details and store it in dataSource vaiable to display on HTML view
    this.orderDetailsService.getOrders().subscribe((res: any) => {
      this.dataSource.data = res;
      console.log( this.dataSource.data);
    });
  }

  //edit row in inline 
  editRow(row: OrderDetails) {
    if (row.id === 0) {
      this.orderDetailsService.addOrder(row).subscribe((newOrder: OrderDetails) => {
        row.id = newOrder.id;
        row.isEdit = false;
      });
    } else {
      this.orderDetailsService.updateOrder(row).subscribe(() => (row.isEdit = false));
    }
  }

  // addRow() {
  //   const newRow: OrderDetails = {
  //     id: 0,
  //     name: '',
  //     state: '',
  //     zip: '',
  //     amount: '',
  //     qty: '',
  //     item: '',
  //     isEdit: true,
  //     isSelected: false,
  //   };
  //   this.dataSource.data = [newRow, ...this.dataSource.data];
  // }


  //remove a record from table
  removeRow(id: number) {
    this.dialog
      .open(ConfirmDialogComponent)
      .afterClosed()
      .subscribe((confirm) => {
        if (confirm) {
          this.orderDetailsService.deleteOrder(id).subscribe(() => {
            this.dataSource.data = this.dataSource.data.filter(
              (u: OrderDetails) => u.id !== id
            );
          });
        }
      });
  }


  inputHandler(e: any, id: number, key: string) {
    if (!this.valid[id]) {
      this.valid[id] = {};
    }
    this.valid[id][key] = e.target.validity.valid;
  }

  disableSubmit(id: number) {
    if (this.valid[id]) {
      return Object.values(this.valid[id]).some((item) => item === false);
    }
    return false;
  }
  
  openPopup() {
    this.dialog.open(AddOrderDetailsComponent).afterClosed().subscribe();
  }


  searchEventHander($event: any) {
    this.searchStringDisplay = $event;
  }


  //filter functionality
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.parentSearchedKey = filterValue;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
}





