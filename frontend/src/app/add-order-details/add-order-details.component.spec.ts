import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrderDetailsComponent } from './add-order-details.component';
import { MatDialog } from '@angular/material/dialog';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';

let matDialogService: jasmine.SpyObj<MatDialog>;
matDialogService = jasmine.createSpyObj<MatDialog>('MatDialog', ['open']);

describe('AddOrderDetailsComponent', () => {
  let component: AddOrderDetailsComponent;
  let fixture: ComponentFixture<AddOrderDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddOrderDetailsComponent ],
      providers:[MatDialog],
      imports:[
               OverlayModule,
               BrowserModule, 
               AppRoutingModule,
               BrowserAnimationsModule,
               MatTableModule,
               MatInputModule,
               MatButtonModule,
               MatDatepickerModule,
               MatNativeDateModule,
               MatDialogModule,
               FormsModule,
               ReactiveFormsModule,
               MatCheckboxModule,
               HttpClientModule,
               OverlayModule,
               MatPaginatorModule,
  ]
    }).compileComponents();

    fixture = TestBed.createComponent(AddOrderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
