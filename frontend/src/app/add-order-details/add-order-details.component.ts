import { Component, OnInit } from '@angular/core';
import { Output, Input, EventEmitter } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { OrderDetails } from '../model/OrderDetails';
import { OrderDetailsService } from '../services/orderDetails.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-add-order-details',
  templateUrl: './add-order-details.component.html',
  styleUrls: ['./add-order-details.component.css']
})
export class AddOrderDetailsComponent implements OnInit {
  submitted = false;
  profileForm: FormGroup = new FormGroup({
                                            name: new FormControl(''),
                                            state: new FormControl(''),
                                            zip: new FormControl(''),
                                            amount: new FormControl(''),
                                            qty: new FormControl(''),
                                            item: new FormControl(''),
                                          });
                                          
  @Output() newItemEvent = new EventEmitter<string>();
  @Input() displayStyle = 'none';

  constructor(public dialog: MatDialog,
             private formBuilder: FormBuilder,
             private orderDetailsService: OrderDetailsService,
             private router: Router, 
             private route: ActivatedRoute) { }

  ngOnInit(): void {
    //initialize order details form 
    this.profileForm = this.formBuilder.group({
          name: ['', [Validators.required,Validators.minLength(3)]],
          state: ['', [Validators.required,Validators.minLength(2)]],
          zip: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(5)]],
          amount: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
          qty: ['', [Validators.required,Validators.pattern("^[0-9]*$")]],
          item: ['', [Validators.required,Validators.minLength(3)]],
      });
  }


  saveOrderDetails() {
    this.submitted = true;
    if (this.profileForm.invalid) {
      return;
    }
    const data = this.profileForm.value;
    this.orderDetailsService.addOrder(data).subscribe((newOrder: OrderDetails) => {
      window.location.reload();
      this.dialog.closeAll();
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.profileForm.controls;
  }

  // closePopup(){
  //   //this.newItemEvent.emit();
  // }

  closePopup() {
    this.dialog.closeAll();
  }
  

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
        console.log(currentUrl);
    });
  }

}
