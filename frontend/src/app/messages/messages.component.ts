import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  @Output() searchString = new EventEmitter<string>();
  
  @Input() searchedKey = '';
  constructor() { }

  ngOnInit(): void {
  }

  searchKey: string = '';

  onSearchKeyword() {
    this.searchString.emit(this.searchKey);
  }

}
