import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { inject } from "@angular/core/testing";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

import { OrderDetailsService } from '../services/orderDetails.service';
import { OrderDetails } from '../model/OrderDetails';

describe('OrderDetailsService', () => {
      let httpTestingController: HttpTestingController;
      let orderAccessService: OrderDetailsService;
      let orderDetails: OrderDetails;
      let baseUrl = 'http://localhost/application-test/backend/index.php';
    beforeEach(() => {
        TestBed.configureTestingModule({
          imports: [HttpClientTestingModule]
    });
      httpTestingController = TestBed.inject(HttpTestingController);  
      orderDetails = {
                id: 1,
                name: "John",
                state: "NY",
                zip: "68009",
                amount: "600",
                qty: "3",
                item: "ITEM034",
                isEdit: true,
                isSelected: false
            };
    });

    beforeEach(inject([OrderDetailsService], (service: OrderDetailsService) => {
             orderAccessService = service;
          }
    ));

    it("should be created", () => {
      expect(orderAccessService).toBeTruthy();
    });

    it("should call GET API to return data", () => {
      let result: OrderDetails[] = [];
      orderAccessService.getOrders().subscribe((res: any) => { result = res; });
      const req = httpTestingController.expectOne({ method: "GET", url: baseUrl+'/order/list' });
      req.flush([orderDetails]);
      expect(result[0]).toEqual(orderDetails);
    });

    it("should call POST API to create a new order", () => {
      orderAccessService.addOrder(orderDetails).subscribe();
      let req = httpTestingController.expectOne({ method: "POST", url: baseUrl+'/order/add' });
      expect(req.request.body).toEqual(orderDetails);
    });

    it("should call PATCH API to update a order", () => {
      orderAccessService.updateOrder(orderDetails).subscribe();
      let req = httpTestingController.expectOne({ method: "PATCH", url: `${baseUrl}/order/edit?id=${orderDetails.id}`});
      expect(req.request.body).toEqual(orderDetails);
    });


    it("should call DELETE order API", () => {
        orderAccessService.deleteOrder(1).subscribe();
        let id = 1;
        let req = httpTestingController.expectOne({ method: "DELETE", url: `${baseUrl}/order/delete?id=${id}` });
        expect(req).toBeDefined();
    });

});
