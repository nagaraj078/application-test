import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrderDetails } from '../model/OrderDetails';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class OrderDetailsService {

  /****************************************************
   *
   * Http Base URL
   *
  *****************************************************/
  private serviceUrl = 'http://localhost/application-test/backend/index.php';


  constructor(private http: HttpClient) {}
  
  /****************************************************
   *
   * Get all the orders from CSV file
   *
  *****************************************************/
  getOrders(): Observable<OrderDetails[]> {
    return this.http.get(`${this.serviceUrl}/order/list`).pipe<OrderDetails[]>(map((data: any) => data));
  }


  /****************************************************
   *
   * Update a order on CSV file
   *
  *****************************************************/
  updateOrder(Order: OrderDetails): Observable<OrderDetails> {
    return this.http.patch<OrderDetails>(`${this.serviceUrl}/order/edit?id=${Order.id}`, Order);
  }

  /****************************************************
   *
   * Add a order into CSV file
   *
  *****************************************************/
  addOrder(Order: OrderDetails): Observable<OrderDetails> {
    return this.http.post<OrderDetails>(`${this.serviceUrl}/order/add`, Order);
  }

  /****************************************************
   *
   * Delete a order from CSV file
   *
  *****************************************************/
  deleteOrder(id: number): Observable<OrderDetails> {
    return this.http.delete<OrderDetails>(`${this.serviceUrl}/order/delete?id=${id}`);
  }

}